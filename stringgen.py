from json import load as load_json
from re import sub
from unidecode import unidecode
from random import choices


class StringGen:
    def __init__(self, file):
        self.lengths = {}
        self.firsts = {}
        self.lasts = {}
        self.letters = {}
        self.create_rule(file)

    def load_data(self, file):
        """ Return clean data from a json file """

        with open(file) as f:
            data = load_json(f)
        for index, word in enumerate(data):
            word = sub(r'\W', '', unidecode(word))
            data[index] = word.lower()
        return data

    def add_entry(self, dictionary, key):
        """ Increment dictionnary entry  or create it """

        if key in dictionary:
            dictionary[key] += 1
        else:
            dictionary[key] = 1

    def add_letter(self, letter, next):
        """ Add next letters probabilities to the rules """

        if letter in self.letters:
            self.add_entry(self.letters[letter], next)
        else:
            self.letters[letter] = {}
            self.add_entry(self.letters[letter], next)

    def create_rule(self, file):
        """ Create rules from the data file """

        data = self.load_data(file)
        for word in data:
            self.add_entry(self.lengths, len(word))
            self.add_entry(self.firsts, word[0])
            self.add_entry(self.lasts, word[-1])

            for i in range(1, len(word), 1):
                self.add_letter(word[i-1], word[i])

    def gen(self):
        """ Generate a random word """

        word = ''

        length = choices(list(self.lengths.keys()),
                         weights=list(self.lengths.values()), k=1)[0]
        first = choices(list(self.firsts.keys()),
                        weights=list(self.firsts.values()), k=1)[0]
        lasts = {}

        for i in range(length):
            if i == 0:
                word += first
            elif word[i-1] not in self.letters:
                break
            else:
                nexts = list(self.letters[word[i-1]].keys())
                weights = list(self.letters[word[i-1]].values())

                if i == length - 1:
                    for letter in nexts:
                        if letter in self.lasts:
                            lasts[letter] = self.lasts[letter]

                    next = choices(list(lasts.keys()),
                                   weights=list(lasts.values()),
                                   k=1)[0]
                else:
                    next = choices(nexts, weights, k=1)[0]

                word += next

        return word
