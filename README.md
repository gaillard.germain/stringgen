# stringgen

## About
stringgen is a small program to generate a string calculated from a given list of words.

## Requirements
Python 3.

## Install
Fork it, clone it.
`python3 demo.py` to run the demo.
